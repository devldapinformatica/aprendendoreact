/**
 * Aula: Stack + Tab
 * React Native App
 * Jonathan Oliveira™
 * Caminho Projeto: cd users/jonathan/desktop/primeiroprojeto
 * Executa App: react-native run-android
 * JSX - Evolução do JavaScript
 **/

 /**
  * Stack + Tab
  * 	Nessa aula iremos utilizar os componentes Stack e Tab Navigator em um unico App
  * 	O componente inicial será o TabNavigator e dentro de uma tela adicionamos um StackNavigator
  * 	
  **/

/**
 * Importando as Bibliotecas Utilizadas
 **/
import React, {
	Component} from 'react';
import {
	View, 
	Text,
	Button,
	TextInput,
	StyleSheet,
	Image}  from 'react-native';

//Criando uma classe
export default class ChatConversa extends Component{

	/**
	 * 'navigationOptions' - Variável estatica utilizada para definir as propriedades do componete de navegação
	 * 		As propriedades utilizadas devem ser compativeis com o componente utilizado
	 * 		Ex: Para cada componente de navegação (TabNavigator/StackNavigator/DrawerNavigator) existe uma propiedade para definição de titulo
	 * 			StackNavigator = headerTitle:'Home'
	 * 			TabNavigator = tabBarLabel:'Home'
	 * 			DrawerNavigator = drawerLabel:'Home'
	 * 		Obs.: De forma geral cada componente possue suas propriedades então é aconselhado que quando utilizado verificar a documentação
	 **/	
	static navigationOptions = ({navigation}) => ({
        title:navigation.state.params.nome,
        tabBarLabel:'ChatConversa',
		tabBarIcon:({tintColor, focused}) => {

			/**
			 * Definindo imagem para o componenete de navegação e alterando a imagem conforme o foco de visualização
			 * 		Obs.: Funciona com os três componentes (TabNavigator/StackNavigator/DrawerNavigator)
			 * 		Obs.: Lembrar sempre de definir o nome da propriedade conforme o componente utilizado
			 * O comando 'focused' identifica a tela atual e define o foco como verdadeiro			 		
			 **/

			//Se foco for verdadeiro
			if(focused){  
				return (
					<Image source={require('../assets/images/chat_on.png')} style={{width:26, height:26}} />
				);

			//Se não	
			}else
				return (
					<Image source={require('../assets/images/chat_off.png')} style={{width:26, height:26}} />
				);
		}
	});

	render(){
		return(
			<View style={styles.viewMain}>
				<Text>
					ChatConversa
				</Text>				
			</View>
		);
	}
}

/**
 * Craição de Constantes
 * 		Constante styles - Nome dado a constante responsável por gerenciar' os styles do aplicativo 
 * 		Obs.: Normalmente em projetos grandes essa Constante fica em arquivo separado
 **/
const styles = StyleSheet.create({
	//Definindo constante para a viewMain deixando os elementos alinhados no centro da tela
	viewMain:{
		flex:1,
		alignItems:'center',
		justifyContent:'center'
	}
});