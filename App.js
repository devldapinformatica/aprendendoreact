/**
 * Aula: Stack + Tab
 * React Native App
 * Jonathan Oliveira™
 * Caminho Projeto: cd users/jonathan/desktop/primeiroprojeto
 * Executa App: react-native run-android
 * JSX - Evolução do JavaScript
 **/

 /**
  * Stack + Tab
  * 	Nessa aula iremos utilizar os componentes Stack e Tab Navigator em um unico App
  * 	O componente inicial será o TabNavigator e dentro de uma tela adicionamos um StackNavigator
  * 	
  **/

/**
 * Importando as Bibliotecas Utilizadas
 **/
import React, {
	Component} from 'react';
import {
	TabNavigator, Image} from 'react-navigation';

/**
 * Importando as Classes Relacionadas
 * Obs.: A classe App irá realizar a comunicação entre as outras telas do aplicativo, essa classe que será responsável pela chamada das telas.
 **/		
import TelaInicial from './src/TelaInicial';
import Config from './src/Config';
import Chat from './src/Chat';

//Removendo avisos do react-native	
console.disableYellowBox=true;

/**
 * Criação de Constantes
 * 		Constante NavigationApp - Nome dado a constante responsável pela navegação entre as telas do APP
 * 		Obs.: Todas as telas do aplicativo devem ser incluídas nessa constante
 **/
const NavigationApp = TabNavigator({
	Home:{
		screen:TelaInicial,	
	},
	Chat: {
		screen:Chat,   		
	},
	Config:{
		screen:Config,
	}		
},	
	/**
	 * Definindo propriedades para o TabNavigator
	 **/
	{
	tabBarOptions: {
		//Define se o ícone será mostrado ou não. Obs.: Padrão para android é false, ou seja, não mostra o ícone
		showIcon: true,
		//Define formato do texto para o titulo. Obs.: Padrão para android é true, ou seja, texto MAIÚSCULO
		upperCaseLabel: false,
		//Define a cor da fonte para o titulo
		activeTintColor: 'red',
			style : {
				//Define a cor de fundo do componente
				backgroundColor: '#FFFFFF',
			},
			labelStyle: {
				//Define tamanho da fonte para o titulo
				fontSize: 10,
			  },
		},
	}
);
export default NavigationApp;